package main

import (
	"net/http"
	"net/url"
	"reflect"
	"testing"
)

const (
	TEST_CONFIG_AUTH1 = "auth1"
)

func emptyRequestWithHeaders(reqUrl string, headers map[string][]string) *http.Request {
	header := http.Header(make(map[string][]string))
	for n, vals := range headers {
		for _, v := range vals {
			header.Add(n, v)
		}
	}
	urlObj, _ := url.Parse(reqUrl)
	return &http.Request{Header: header, URL: urlObj}
}

func TestAuthIllegalType(t *testing.T) {
	auth := NewAuthChecker(ConfigAuth{Type: "illegal"})
	if auth != nil {
		t.Fatalf("NewAuthChecker with type illegal - expected nil, got non-nil")
	}
}

func TestAuthGitlabNoToken(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}

	delete(config.Auths[TEST_CONFIG_AUTH1].Params, GITLAB_TOKEN_PARAM)
	testAuthGitlab(t, config.Auths[TEST_CONFIG_AUTH1])
}

func TestAuthGitlab(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}

	testAuthGitlab(t, config.Auths[TEST_CONFIG_AUTH1])
}

func testAuthGitlab(t *testing.T, configAuth ConfigAuth) {
	auth := NewAuthChecker(configAuth)
	if auth == nil {
		t.Fatalf("NewAuthChecker with type illegal - expected nil, got non-nil")
	}
	gitlabAuth, ok := auth.(*GitlabAuthChecker)
	if !ok {
		t.Fatalf("expected auth type *GitlabAuthChecker, got %s", reflect.TypeOf(auth))
	}
	token := ""
	if configAuth.Params != nil {
		token = configAuth.Params[GITLAB_TOKEN_PARAM]
	}
	if gitlabAuth.token != token {
		t.Errorf("expected gitlab token %s, got %s", token, gitlabAuth.token)
	}

	r := emptyRequestWithHeaders("", map[string][]string{GITLAB_AUTH_HEADER: []string{"1234"}})
	result, err := gitlabAuth.AuthCheck(r)
	if err != nil {
		t.Errorf("unexpected error in AuthCheck - %s", err)
	}
	if result {
		t.Errorf("unexpected AuthCheck result, expected %v got %v", false, true)
	}

	r = emptyRequestWithHeaders("", map[string][]string{})
	result, err = gitlabAuth.AuthCheck(r)
	if err != MISSING_GITLAB_AUTH_HEADER {
		t.Errorf("expected error '%s' in AuthCheck, got %s", MISSING_GITLAB_AUTH_HEADER, err)
	}
	if result {
		t.Errorf("unexpected AuthCheck result, expected %v got %v", false, true)
	}

	r = emptyRequestWithHeaders("", map[string][]string{GITLAB_AUTH_HEADER: []string{"1234", "123"}})
	result, err = gitlabAuth.AuthCheck(r)
	if err != MULTIPLE_GITLAB_AUTH_HEADER {
		t.Errorf("expected error '%s' in AuthCheck, got %s", MULTIPLE_GITLAB_AUTH_HEADER, err)
	}
	if result {
		t.Errorf("unexpected AuthCheck result, expected %v got %v", false, true)
	}

	r = emptyRequestWithHeaders("", map[string][]string{GITLAB_AUTH_HEADER: []string{configAuth.Params[GITLAB_TOKEN_PARAM]}})
	result, err = gitlabAuth.AuthCheck(r)
	if err != nil {
		t.Errorf("unexpected error %s in AuthCheck", err)
	}
	if !result {
		t.Errorf("unexpected AuthCheck result, expected %v got %v", true, false)
	}
}
