package main

import (
	"io/ioutil"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestHandlerIllegalType(t *testing.T) {
	h := NewHandler(ConfigHandler{Type: "illegal"})
	if h != nil {
		t.Fatalf("NewHandler with type illegal - expected nil, got non-nil")
	}
}

func TestCmdHandlerNoParam(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}

	delete(config.Endpoints[0].Handlers[0].Params, HANDLER_CMD_COMMAND)
	h := NewHandler(config.Endpoints[0].Handlers[0])
	if h != nil {
		t.Fatalf("NewHandler with type cmd and no cmd param - expected nil, got non-nil")
	}

	config.Endpoints[0].Handlers[0].Params = nil
	h = NewHandler(config.Endpoints[0].Handlers[0])
	if h != nil {
		t.Fatalf("NewHandler with type cmd and no params - expected nil, got non-nil")
	}
}

func TestCmdHandlerExecute(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}

	oldwd, err := os.Getwd()
	err = os.Chdir(oldwd + "/" + config.HandlersPath)
	for _, ep := range config.Endpoints {
		for _, h := range ep.Handlers {
			handler := NewHandler(h)
			if handler == nil {
				t.Errorf("NewHandler - expected non-nil, got nil")
			}
			_, ok := handler.(*CommandHandler)
			if !ok {
				t.Errorf("expected handler type *CommandHandler, got %s", reflect.TypeOf(handler))
			}
			res, err := handler.Execute()
			if err != nil {
				t.Errorf("unexpected error %s when executing handler %s", err, h.Params[HANDLER_CMD_COMMAND])
			}
			outFile := strings.TrimPrefix(h.Params[HANDLER_CMD_COMMAND], "./") + ".out"
			expectedRes, err := ioutil.ReadFile(outFile)
			if err != nil {
				t.Error(os.Getwd())
				t.Fatalf("missing expected output file %s", outFile)
			}
			if string(expectedRes) != res {
				t.Errorf("wrong output for handler %s\nexpected:\n%s\ngot:\n%s", h.Params[HANDLER_CMD_COMMAND], expectedRes, res)
			}
		}
	}
	os.Chdir(oldwd)
}
