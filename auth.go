package main

import (
	"errors"
	"fmt"
	"net/http"
)

const (
	GITLAB_AUTH_HEADER = "X-Gitlab-Token"
	GITLAB_TOKEN_PARAM = "token"
)

var (
	MULTIPLE_GITLAB_AUTH_HEADER = errors.New(fmt.Sprintf("multiple Gitlab authentication headers %s unsupported", GITLAB_AUTH_HEADER))
	MISSING_GITLAB_AUTH_HEADER  = errors.New(fmt.Sprintf("missing Gitlab authentication header %s", GITLAB_AUTH_HEADER))
)

type AuthChecker interface {
	AuthCheck(*http.Request) (bool, error)
}

type GitlabAuthChecker struct {
	token string
}

// Create new Gitlab authentication checker that checks incoming requests to
// have correct Gitlab authentication token
func NewGitlabAuthChecker(token string) *GitlabAuthChecker {
	return &GitlabAuthChecker{
		token: token,
	}
}

// Check that request has correct Gitlab authentication token
func (auth *GitlabAuthChecker) AuthCheck(req *http.Request) (bool, error) {
	if v, exists := req.Header[GITLAB_AUTH_HEADER]; exists {
		if len(v) != 1 {
			return false, MULTIPLE_GITLAB_AUTH_HEADER
		}
		return v[0] == auth.token, nil
	}
	return false, MISSING_GITLAB_AUTH_HEADER
}

func NewAuthChecker(conf ConfigAuth) AuthChecker {
	switch conf.Type {
	case "gitlab":
		token := ""
		if conf.Params != nil {
			token = conf.Params[GITLAB_TOKEN_PARAM]
		}
		return NewGitlabAuthChecker(token)
	}
	return nil
}
