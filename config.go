package main

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

const (
	ConfigHandlerTypeCommand = "cmd"
)

type Config struct {
	Port         int                   `yaml:"port"`
	HandlersPath string                `yaml:"handlersPath"`
	Auths        map[string]ConfigAuth `yaml:"auths"`
	Endpoints    []ConfigEndpoint      `yaml:"endpoints"`
}

type ConfigEndpoint struct {
	Path     string          `yaml:"path"`
	Auth     string          `yaml:"auth"`
	Handlers []ConfigHandler `yaml:"handlers"`
}

type ConfigAuth struct {
	Type   string            `yaml:"type"`
	Params map[string]string `yaml:"params"`
}

type ConfigHandler struct {
	Type   string            `yaml:"type"`
	Params map[string]string `yaml:"params"`
}

func parseConfig(configFile string) (*Config, error) {
	result := &Config{}
	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(data, result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
