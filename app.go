package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

type App struct {
	Endpoints map[string]*Endpoint
	Server    *http.Server
	ServeMux  *http.ServeMux
	Port      int
}

type Endpoint struct {
	AuthChecker AuthChecker
	Handlers    []Handler
}

func NewApp(config *Config) (*App, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	app := &App{
		Endpoints: make(map[string]*Endpoint),
		Port:      config.Port,
		ServeMux:  http.NewServeMux(),
		Server: &http.Server{
			Addr: fmt.Sprintf(":%d", config.Port),
		},
	}
	app.Server.Handler = app.ServeMux
	auths := make(map[string]AuthChecker)
	for name, authConfig := range config.Auths {
		auths[name] = NewAuthChecker(authConfig)
		if auths[name] == nil {
			return nil, errors.New(fmt.Sprintf("unknown auth type %s", authConfig.Type))
		}
	}
	for _, ep := range config.Endpoints {
		app.ServeMux.HandleFunc(ep.Path, app.HttpHandler)
		app.Endpoints[ep.Path] = &Endpoint{}
		app.Endpoints[ep.Path].AuthChecker = auths[ep.Auth]
		app.Endpoints[ep.Path].Handlers = make([]Handler, len(ep.Handlers))
		for j, h := range ep.Handlers {
			app.Endpoints[ep.Path].Handlers[j] = NewHandler(h)
			if app.Endpoints[ep.Path].Handlers[j] == nil {
				return nil, errors.New(fmt.Sprintf("unknown handler type %s", h))
			}
		}
	}
	if strings.HasPrefix(config.HandlersPath, "/") {
		os.Chdir(config.HandlersPath)
	} else {
		os.Chdir(wd + "/" + config.HandlersPath)
	}
	return app, nil
}

// Main HTTP handler
func (app *App) HttpHandler(w http.ResponseWriter, r *http.Request) {
	endpoint, ok := app.Endpoints[r.URL.Path]
	if !ok {
		http.NotFound(w, r)
		return
	}
	authed, err := endpoint.AuthChecker.AuthCheck(r)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	if !authed {
		http.NotFound(w, r)
		return
	}
	for i, handler := range endpoint.Handlers {
		output, err := handler.Execute()
		log.Printf("endpoint %s handler %d output:\n%s", r.URL.Path, i, output)
		if err != nil {
			log.Printf("error executing handler %d for endpoint %s - %s", i, r.URL.Path, err)
			continue
		}
		log.Printf("successfully executed handler %d for endpoint %s", i, r.URL.Path)
	}
}

// Run application main loop
func (app *App) Run() error {
	log.Printf("Listening on port %d...", app.Port)
	return app.Server.ListenAndServe()
}
