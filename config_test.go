package main

import (
	"testing"
)

const (
	TEST_FIXTURES_DIR = "test-fixtures"
)

func fixtureFile(file string) string {
	return TEST_FIXTURES_DIR + "/" + file
}

func TestConfig(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_illegal_config.yaml"))
	if err == nil {
		t.Errorf("test_illegal_config.yaml - expected error, got nil")
	}

	config, err = parseConfig(fixtureFile("unexisting_config.yaml"))
	if err == nil {
		t.Fatal(err)
	}

	config, err = parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}

	if config.Port != 1000 {
		t.Errorf("wrong port in test_config.yaml expected %d got %d", 1000, config.Port)
	}
}
