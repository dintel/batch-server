package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf(" This is a simple batch server that parses incoming requests and runs handlers on them.\n\n")
		fmt.Printf("Usage: %s <configuration-file>\n", os.Args[0])
		os.Exit(1)
	}

	config, err := parseConfig(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	app, err := NewApp(config)
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(app.Run())
}
