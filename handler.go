package main

import (
	"bytes"
	"os/exec"
)

const (
	HANDLER_CMD_COMMAND = "cmd"
)

type Handler interface {
	Execute() (string, error)
}

type CommandHandler struct {
	Command string
}

func NewCommandHandler(cmd string) *CommandHandler {
	return &CommandHandler{
		Command: cmd,
	}
}

// Execute command handler - run command.
func (handler *CommandHandler) Execute() (string, error) {
	cmd := exec.Command(handler.Command)
	var outbuf, errbuf bytes.Buffer
	cmd.Stdout = &outbuf
	cmd.Stderr = &errbuf
	err := cmd.Run()
	return "---STDOUT---\n" + outbuf.String() + "\n---STDERR---\n" + errbuf.String(), err
}

func NewHandler(conf ConfigHandler) Handler {
	switch conf.Type {
	case "cmd":
		if conf.Params == nil {
			return nil
		}
		if _, ok := conf.Params[HANDLER_CMD_COMMAND]; !ok {
			return nil
		}
		return NewCommandHandler(conf.Params[HANDLER_CMD_COMMAND])
	}
	return nil
}
