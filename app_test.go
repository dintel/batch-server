package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestNewApp(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}

	wd, _ := os.Getwd()
	app, err := NewApp(config)
	if err != nil {
		t.Fatal(err)
	}
	os.Chdir(wd)

	if len(app.Endpoints) != len(config.Endpoints) {
		t.Errorf("wrong number of endpoints in app - expected %d, got %d", len(config.Endpoints), len(app.Endpoints))
	}

	config.Auths["illegalAuth"] = ConfigAuth{Type: "illegalAuth"}
	_, err = NewApp(config)
	if err == nil {
		t.Errorf("NewApp with illegal auth type, expected error, got nil ")
	}

	delete(config.Auths, "illegalAuth")
	config.Endpoints[0].Handlers[0].Type = "illegelType"
	_, err = NewApp(config)
	if err == nil {
		t.Errorf("NewApp with illegal handler type, expected error, got nil ")
	}
}

// TestAppHandler tests application HTTP handler
func TestAppHandler(t *testing.T) {
	config, err := parseConfig(fixtureFile("test_config.yaml"))
	if err != nil {
		t.Fatal(err)
	}
	config.Endpoints[1].Handlers = append(config.Endpoints[1].Handlers, ConfigHandler{Type: ConfigHandlerTypeCommand, Params: map[string]string{HANDLER_CMD_COMMAND: "illegal"}})

	wd, _ := os.Getwd()
	app, err := NewApp(config)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	req := emptyRequestWithHeaders("http://test.local/", nil)
	app.HttpHandler(w, req)
	if w.Code != http.StatusNotFound {
		t.Errorf("expected HTTP code 404, got %d", w.Code)
	}

	w = httptest.NewRecorder()
	req = emptyRequestWithHeaders("http://test.local", map[string][]string{GITLAB_AUTH_HEADER: {"1", "2"}})
	app.HttpHandler(w, req)
	if w.Code != http.StatusNotFound {
		t.Errorf("expected HTTP code 404, got %d", w.Code)
	}

	w = httptest.NewRecorder()
	req = emptyRequestWithHeaders("http://test.local"+config.Endpoints[0].Path, map[string][]string{GITLAB_AUTH_HEADER: {"1", "2"}})
	app.HttpHandler(w, req)
	if w.Code != http.StatusInternalServerError {
		t.Errorf("expected HTTP code 500, got %d", w.Code)
	}

	w = httptest.NewRecorder()
	req = emptyRequestWithHeaders("http://test.local"+config.Endpoints[0].Path, nil)
	app.HttpHandler(w, req)
	if w.Code != http.StatusInternalServerError {
		t.Errorf("expected HTTP code 500, got %d", w.Code)
	}

	w = httptest.NewRecorder()
	req = emptyRequestWithHeaders("http://test.local"+config.Endpoints[0].Path, map[string][]string{GITLAB_AUTH_HEADER: {"1"}})
	app.HttpHandler(w, req)
	if w.Code != http.StatusNotFound {
		t.Errorf("expected HTTP code 404, got %d", w.Code)
	}

	w = httptest.NewRecorder()
	req = emptyRequestWithHeaders("http://test.local"+config.Endpoints[0].Path, map[string][]string{GITLAB_AUTH_HEADER: {config.Auths[config.Endpoints[0].Auth].Params[GITLAB_TOKEN_PARAM]}})
	app.HttpHandler(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("expected HTTP code 200, got %d", w.Code)
	}

	w = httptest.NewRecorder()
	req = emptyRequestWithHeaders("http://test.local"+config.Endpoints[1].Path, map[string][]string{GITLAB_AUTH_HEADER: {config.Auths[config.Endpoints[1].Auth].Params[GITLAB_TOKEN_PARAM]}})
	app.HttpHandler(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("expected HTTP code 200, got %d", w.Code)
	}
	os.Chdir(wd)
}
